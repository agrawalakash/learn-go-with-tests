package iterations

import "testing"
import "github.com/stretchr/testify/assert"

func TestValidateQueryParams(t *testing.T) {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			actualString := repeat("a", 5)
			expectedString := "aaaaa"
			assert.Equal(t, actualString, expectedString)
		})
	})
}

func TestValidateQueryForParams(t *testing.T) {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			actualInt := count()
			expectedInt := 2
			assert.Equal(t, actualInt, expectedInt)
		})
	})
}
