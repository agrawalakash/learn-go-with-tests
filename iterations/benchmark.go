package iterations

import "testing"

func TestBenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		repeat("a", 4)
	}
}