package maps

import "testing"

func TestSearch(t *testing.T) {
	t.Run("known word", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		got, _ := dictionary.Search("test")
		want := "this is just a test"
		assertStrings(got, want, t)
	})
	t.Run("unknown word", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		_, error := dictionary.Search("unknown")
		want := "could not find the word you were looking for"
		if error == nil {
			t.Fatal("expected to get an error.")
		}
		assertStrings(error.Error(), want, t)
	})
}

func TestAdd(t *testing.T) {
	t.Run("add new word", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		dictionary.Add("test2", "this is just an addition")
		got, error := dictionary.Search("test2")
		assertDefinition(error, got, t, "this is just an addition")
	})

	t.Run("existing word", func(t *testing.T){
		dictionary := Dictionary{"test": "this is just a test"}
		dictionary.Add("test", "this is just an addition")
		got, error := dictionary.Search("test")
		assertDefinition(error, got, t, "this is just a test")
	})
}

func TestUpdate(t *testing.T)  {
	t.Run("update existing word", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		dictionary.Update("test", "this is an updation")
		got, error := dictionary.Search("test")
		assertDefinition(error, got, t, "this is an updation")
	})
}

func TestDelete(t *testing.T)  {
	t.Run("delete existing word", func(t *testing.T) {
		query := "test"
		dictionary := Dictionary{query : "this is just a test"}
		dictionary.Delete(query)
		got, error := dictionary.Search(query)
		assertDefinition(error, got, t, "this is just a test")
	})
}

func assertDefinition(error error, got string, t *testing.T, wantNoError string) {
	if error == nil {
		want := wantNoError
		assertStrings(got, want, t)
	}

	if error != nil {
		want := "could not find the word you were looking for"
		assertStrings(error.Error(), want, t)
	}
}

func assertStrings(got string, want string, t *testing.T) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q given, %q", got, want, "test")
	}
}
