package maps

type Dictionary map[string]string

const(
ErrNotFound = DictionaryErr("could not find the word you were looking for")
ErrWordExists = DictionaryErr("cannot add words because it already exists")
ErrWordDoesNotExists = DictionaryErr("cannot update words because it does not exists")
)

type DictionaryErr string

func (e DictionaryErr) Error() string {
	return string(e)
}
func (d Dictionary) Search(query string) (string, error) {
	definition, ok := d[query]
	if !ok {
		return "", ErrNotFound
	}
	return definition, nil
}

func (d Dictionary) Add(query string, result string) error {
	_, err := d.Search(query)
	switch err {
	case ErrNotFound:
		d[query] = result
	case nil:
		return ErrWordExists
	default:
		return err
	}
	return nil
}

func (d Dictionary) Update(query string, result string) error {
	_, err := d.Search(query)
	switch err {
	case nil:
		d[query] = result
	case ErrNotFound:
		return ErrWordDoesNotExists
	default:
		return err
	}
	return nil
}

func (d Dictionary) Delete(query string) {
	delete(d, query)
}


