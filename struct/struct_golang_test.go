package _struct

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRectangleArea(t *testing.T) {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			rectangle := Rectangle{5, 4}
			area := rectangle.area()
			expectedArea := 20.0
			assert.Equal(t, area, expectedArea)
		})
	})
}

func TestCircleArea(t *testing.T){
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			circle := Circle{5}
			area := circle.area()
			expectedArea := 25.0
			assert.Equal(t, area, expectedArea)
		})
	})
}

func TestCheckArea(t *testing.T)  {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			circle := Circle{5}
			var shape Shape = circle
			area := shape.area()
			expectedArea := 25.0
			assert.Equal(t, area, expectedArea)
		})
	})
}

func TestCheckAreaRectangle(t *testing.T)  {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			rectangle := Rectangle{5, 4}
			var shape Shape = rectangle
			area := shape.area()
			expectedArea := 20.0
			assert.Equal(t, area, expectedArea)
		})
	})
}

func TestCheckPerimeterShapeRectangle(t *testing.T)  {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			rectangle := Rectangle{5, 4}
			perimeter := getPerimeter(rectangle)
			expectedPerimeter := 18.0
			assert.Equal(t, perimeter, expectedPerimeter)
		})
	})
}
