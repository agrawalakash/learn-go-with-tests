package _struct



type Shape interface{
	area() float64
	perimeter() float64
}

type Rectangle struct {
	length float64
	breadth float64
}

type Circle struct {
	radius float64
}


func (r Rectangle) area() float64{
	return r.length * r.breadth
}

func (r Rectangle) perimeter() float64{
	return 2 * (r.length + r.breadth)
}

func (c Circle)  area() float64{
	return c.radius * c.radius
}

func (c Circle)  perimeter() float64{
	return 2 * 3.14 * c.radius
}

func getPerimeter(shape Shape)  float64{
	return shape.perimeter()
}


