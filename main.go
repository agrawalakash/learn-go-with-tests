package main

import (
	"fmt"
	"io"
	. "main/mocking"
	"net/http"
	"os"
	. "time"
)

func Greet(writer io.Writer, name string) {
	fmt.Fprintf(writer, "Hello, %s", name)
}

func MyGreeterHandler(w http.ResponseWriter, r *http.Request) {
	Greet(w, "world")
}

func main() {
	//http.ListenAndServe(":5000", http.HandlerFunc(MyGreeterHandler))
	sleeper := &DefaultSleeper{}
	// os.Stdout writes to the console itself while buffer doesnt
	Countdown(os.Stdout, sleeper)

	configurableSleeper := &ConfigurableSleeper{Duration: 1* Second, SleepFunc: Sleep}
	Countdown(os.Stdout, configurableSleeper)
}