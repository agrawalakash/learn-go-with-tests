package sum_n_elements

func sum(numbers[5] int) int {
	var sum int
	//for i :=0 ; i<5 ; i++{
	//	sum = sum + numbers[i]
	//}

	for _, number := range numbers{
		sum += number
	}

	return sum
}

func sumSlice(numbers[] int) int {
	var sum int
	//for i :=0 ; i<5 ; i++{
	//	sum = sum + numbers[i]
	//}

	for _, number := range numbers{
		sum += number
	}

	return sum
}