package sum_n_elements

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestValidateQueryParams(t *testing.T) {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			numbers := [5]int{1, 2, 3, 4, 5}
			actualSum := sum(numbers)
			expectedSum := 15
			assert.Equal(t, actualSum, expectedSum)
		})
	})
}

func TestValidateSliceQueryParams(t *testing.T) {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			numbers := []int{1, 2, 3, 4, 5}
			actualSum := sumSlice(numbers)
			expectedSum := 15
			assert.Equal(t, actualSum, expectedSum)
		})
	})
}

func TestValidateSliceEmptyQueryParams(t *testing.T) {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			var numbers []int
			actualSum := sumSlice(numbers)
			expectedSum := 0
			assert.Equal(t, actualSum, expectedSum)
		})
	})
}

func TestValidateSlicesQueryParams(t *testing.T) {
	t.Run("when expected is not same as actual", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			numbers := []int{1, 2, 3, 4, 5, 6, 7}
			actualSum := sumSlice(numbers[2:5])
			expectedSum := 15
			assert.Equal(t, actualSum, expectedSum)
		})
	})
}

