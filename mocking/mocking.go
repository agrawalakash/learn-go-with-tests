package mocking

import (
	"fmt"
	"io"
	"time"
)

const countDownStart = 3
const finalWord = "Go!"
const write = "write"
const sleep = "sleep"

// In collaborative projects there is value auto-generating mocks. For example we use mockito in android for autoGenerating mocks
type Sleeper interface {
	Sleep()
}

type SpySleeper struct {
	Calls int
}

type DefaultSleeper struct {}

type ConfigurableSleeper struct {
	Duration time.Duration
	SleepFunc func(time.Duration)
}

type CountdownOperationsSpy struct {
	Calls []string
}

type SpyTime struct {
	durationSlept time.Duration
}

// SpySleeper extends our interface Sleeper as it implements its method sleep
func (s *SpySleeper) Sleep() {
	s.Calls++
}

// DefaultSleeper extends our interface Sleeper as it implements its method sleep
func (d *DefaultSleeper) Sleep() {
	time.Sleep(1 * time.Second)
}

// ConfigurableSleeper extends our interface Sleeper as it implements its method sleep
func (c *ConfigurableSleeper) Sleep() {
	c.SleepFunc(c.Duration)
}

// CountdownOperationsSpy extends our interface Sleeper as it implements its method sleep
func (s *CountdownOperationsSpy) Sleep() {
	s.Calls = append(s.Calls, sleep)
}

// CountdownOperationsSpy extends the interface Writer as it implements its method write
func (s *CountdownOperationsSpy) Write(p []byte) (n int, err error) {
	s.Calls = append(s.Calls, write)
	return
}

func (s *SpyTime) Sleep(duration time.Duration) {
	s.durationSlept = duration
}

func Countdown(out io.Writer, sleeper Sleeper)  {
	//fmt.Fprint(out, "3")
	for i:= countDownStart; i>0; i-- {
		sleeper.Sleep()
		fmt.Fprintln(out, i)
	}
	sleeper.Sleep()
	fmt.Fprint(out, finalWord)
}