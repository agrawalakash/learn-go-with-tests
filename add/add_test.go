package add


import "testing"
import "github.com/stretchr/testify/assert"

func TestValidateQueryParams(t *testing.T) {
	t.Run("when params is empty", func(t *testing.T) {
		t.Run("it should return an error", func(t *testing.T) {
			actualSum := adder(1, 2)
			expectedSum := 3
			assert.Equal(t, actualSum, expectedSum)
		})
	})
}